class Empleado:
    def __init__(self, n, s):  #self es una variable "imaginaria"
        self.nombre = n
        self.nomina = s

    def calcula_impuestos(self):
        return self.nomina * 0.30

    def __str__(self):
        return "El empleado {name} debe pagar {tax:.2f}".format(name = self.nombre, 
        tax = self.calcula_impuestos())


class Jefe(Empleado):
    def __init__(self, n, s, e):
        super().__init__(n, s)
        self.extra = e
    def calcula_impuestos1(self):
        return (self.nomina + self.extra) * 0.30
    def calcula_impuestos(self):
        return super().calcula_impuestos() + self.extra * 0.30
    #dejo que mi padre trabaje y ñuego sumo yo lo mio
    # si detectasemos algun error en el calculo y tuviesemosque modificar el empleado, esta forma saldria bien mientras que la anterior mal   
    def __str__(self):
        return "El jefe {name} debe pagar {tax:.2f}".format(name = self.nombre, 
        tax = self.calcula_impuestos())
  

empleado = Empleado("Pepe", 20000)
jefe = Jefe("Ana", 30000, 2000)
print (empleado)
print (jefe)