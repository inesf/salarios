def CosteSalarial(n, i):
    return (n + i)

class Empleado:
    def __init__(self, n, s):  #self es una variable "imaginaria"
        self.nombre = n
        self.nomina = s

    def calcula_impuestos(self):
        return self.nomina * 0.30

    def __str__(self):
        return "El empleado {name} debe pagar {tax:.2f}".format(name = self.nombre, tax = self.calcula_impuestos())


empleadoPepe = Empleado("Pepe", 20000)
empleadoAna = Empleado("Ana", 30000)

total = empleadoPepe.calcula_impuestos() + empleadoAna.calcula_impuestos()

print(empleadoPepe)
print(empleadoAna)
print("Los impuestos a pagar en total son {: .2f} euros".format(total))



