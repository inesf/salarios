class Empleado:
    def __init__(self, n, s):  #self es una variable "imaginaria"
        self.nombre = n
        self.nomina = s

    def calcula_impuestos(self):
        return self.nomina * 0.30

    def __str__(self):
        return "El empleado {name} debe pagar {tax:.2f}".format(name = self.nombre, tax = self.calcula_impuestos())