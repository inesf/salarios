# Salarios
from re import I


def CosteSalarial(n, i):
    return (n + i)


#init es un constructor
class Empleado:
    def __init__(self, n, s):  #self es una variable "imaginaria"
        self.nombre = n
        self.nomina = s

    def calculo_impuestos(self):
        return self.nomina * 0.30

    def __str__(self):
        return "El empleado {name} debe pagar {tax:.2f}".format(name = self.nombre, tax = self.calculo_impuestos())


empleadoPepe = Empleado("Pepe", 20000)
empleadoAna = Empleado("Ana", 30000)

total = empleadoPepe.calculo_impuestos() + empleadoAna.calculo_impuestos()

empleadoPepe.imprime()
empleadoAna.imprime()
print("los impuestos a pagar en total son {: .2f} euros".format(total))


lista_nominas = [30000, 20000, 10000, 5000, 80000, 35000, 27000, 90000, 4000, 100000]
lista_porcentajes = [0.3, 0.2, 0.1, 0.25, 0.4, 0.5, 0.33, 0.15, 0.005, 0.35]
lista_antiguedad = ["Yes", "No", "Yes", "No", "Yes", "No", "Yes", "No", "Yes", "No"]
for i in range (len(lista_nominas) - 1):
    nomina = lista_nominas[i]
    porcentaje = lista_porcentajes[i]
    impuestos = Impuesto(nomina, porcentaje)
    costesalarial = CosteSalarial(nomina, impuestos)
    if lista_antiguedad == "Yes":
        costesalarial1 = costesalarial * 0.9
    else:
        costesalarial1 = costesalarial
    empleado = i
    print ("Empleado ", empleado)
    print ("Su nomina es:", nomina)
    print ("Los impuestos a pagar son:", impuestos)
    print ("El coste salarial es: ", costesalarial1)

#Hola
